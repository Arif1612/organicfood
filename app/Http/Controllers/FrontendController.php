<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Http\Controllers\FrontendController;

class FrontendController extends Controller
{
    public function showIndex(){
        return view('frontend.index');
    }
    public function showCategoryList(){
        return view('frontend.categoryList');
    }
    public function showCheckout(){
        return view('frontend.checkout');
    }
    public function showSingleProductDetails(){
        return view('frontend.singleProductDetails');
    }
}
