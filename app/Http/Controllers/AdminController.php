<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class adminController extends Controller
{
  public function index()
  {
    return view('admin.index');
  }
  public function categoryList()
  {
    return view('admin.categoryList');
  }
  public function productList()
  {
    return view('admin.productList');
  }
  public function userList()
  {
    return view('admin.userList');
  }

}
