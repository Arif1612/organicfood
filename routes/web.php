<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontendController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
    return view ('welcome');
});

Route::get('/index',[AdminController::class,'index']);
Route::get('/productlist',[AdminController::class,'productList']);
Route::get('/categorylist',[AdminController::class,'categoryList']);
Route::get('/userlist',[AdminController::class,'userList']);
Route::get('/findex',[FrontendController::class,'showIndex']);
Route::get('/fcategorylist',[FrontendController::class,'showCategoryList']);
Route::get('/fcheckout',[FrontendController::class,'showCheckout']);
Route::get('/fsingleproductdetails',[FrontendController::class,'showSingleProductDetails']);



