<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Login</title>

	<link href="resources/favicon.png" rel="icon">

	<!-- Bootstrap CSS (CDN)-->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />
	<link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css" rel="stylesheet"
		crossorigin="anonymous">

	<!-- Simple DataTable (CDN) -->
	<link href="https://cdn.jsdelivr.net/npm/simple-datatables@3.2.2/dist/style.css" rel="stylesheet"
		integrity="sha256-ZerMjX+PoTwR33srlBlYteG2MwTBUFimpp4wcT1w/lg=" crossorigin="anonymous">

	<!-- Bootstrap CSS (Local) -->
	<!-- <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet"> -->

	<!-- Simple DataTable (Local) -->
	<!-- <link href="assets/vendor/simple-datatables/style.css" rel="stylesheet"> -->

	<!-- Custom CSS -->
	<link href="css/style.css" rel="stylesheet">

	<!-- Font Awesome Kit JS -->
	<!-- <script src="https://kit.fontawesome.com/496c26838e.js" crossorigin="anonymous"></script> -->
</head>

<body>

  <main>
    <div class="container">

      <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

              <div class="d-flex justify-content-center py-4">
                <a class="logo d-flex align-items-center w-auto">
                  <img src="resources/logo.png" alt="">
                  <span class="d-none d-lg-block text-green">Organic Food</span>
                </a>
              </div><!-- End Logo -->

              <div class="card mb-3">

                <div class="card-body">

                  <div class="pt-4 pb-2">
                    <h5 class="card-title text-center pb-0 fs-4">Login to Your Account</h5>
                    <p class="text-center small">Enter your email address & password to login</p>
                  </div>

                  <form class="row g-3 p-3 needs-validation" action="" method="GET">
                    <div class="col-12">
                      <div class="form-floating mb-3">
                        <input name="email" type="email" class="form-control" id="user_email" placeholder="name@example.com"
                          autofocus required>
                        <label for="user_email">Email address</label>
                      </div>
                    </div>

                    <div class="col-12">
                      <div class="form-floating mb-3">
                        <input name="password" type="password" class="form-control" id="user_password" name="user_password"
                          placeholder="" required>
                        <label for="user_password">Password</label>
                      </div>          
                    </div>

                    <div class="col-12">
                      <button class="btn btn-primary w-100" type="submit">Login</button>
                    </div>
                    <div class="col-12">
                      <p class="small mb-0">Don't have account? <a class="text-black" href="register.html">Create an account</a></p>
                    </div>
                  </form>

                </div>
              </div>

            </div>
          </div>
        </div>
      </section>

    </div>
  </main>
	
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

	<!-- Bootstrap bundle (CDN) -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
		crossorigin="anonymous"></script>

	<!-- Simple DataTables (CDN) -->
	<script src="https://cdn.jsdelivr.net/npm/simple-datatables@3.2.2/dist/umd/simple-datatables.js"
		integrity="sha256-Usm730G3l59Ux42era3GIRJOYXFLU7K9k7JFInXTeG0=" crossorigin="anonymous"></script>

	<!-- ChartJS (CDN) -->
	<script src="https://cdn.jsdelivr.net/npm/chart.js@3.8.0/dist/chart.min.js"
		integrity="sha256-cHVO4dqZfamRhWD7s4iXyaXWVK10odD+qp4xidFzqTI=" crossorigin="anonymous"></script>

	<!-- Bootstrap bundle (Local) -->
	<!-- <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->

	<!-- Simple DataTables (Local) -->
	<!-- <script src="assets/vendor/simple-datatables/simple-datatables.js"></script> -->

	<!-- ChartJs (Local) -->
	<!-- <script src="assets/vendor/chart.js/chart.min.js"></script> -->

	<!-- Custom JS -->
	<script src="js/main.js"></script>
</body>
</html>
