<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>View User Statistics</title>

	<link href="resources/favicon.png" rel="icon">

	<!-- Bootstrap CSS (CDN)-->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />
	<link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css" rel="stylesheet"
		crossorigin="anonymous">

	<!-- Simple DataTable (CDN) -->
	<link href="https://cdn.jsdelivr.net/npm/simple-datatables@3.2.2/dist/style.css" rel="stylesheet"
		integrity="sha256-ZerMjX+PoTwR33srlBlYteG2MwTBUFimpp4wcT1w/lg=" crossorigin="anonymous">

	<!-- Bootstrap CSS (Local) -->
	<!-- <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet"> -->

	<!-- Simple DataTable (Local) -->
	<!-- <link href="assets/vendor/simple-datatables/style.css" rel="stylesheet"> -->

	<!-- Custom CSS -->
	<link href="css/style.css" rel="stylesheet">

	<!-- Font Awesome Kit JS -->
	<!-- <script src="https://kit.fontawesome.com/496c26838e.js" crossorigin="anonymous"></script> -->
</head>

<body>
	<header id="header" class="header fixed-top d-flex align-items-center">
		<!-- Start Logo -->
		<div class="d-flex align-items-center justify-content-between">
			<a href="index.html" class="logo d-flex align-items-center">
				<img src="resources/logo.png" alt="">
				<span class="d-none d-lg-block text-green">Organic Food</span>
			</a>
			<i class="bi bi-list toggle-sidebar-btn"></i>
		</div>
		<!-- End Logo -->

		<!-- Start Search Bar -->
		<div class="search-bar ms-auto">
			<form class="search-form d-flex align-items-center" method="POST" action="#">
				<input type="text" name="query" placeholder="Search" title="Enter search keyword">
				<button type="submit" title="Search"><i class="bi bi-search"></i></button>
			</form>
		</div>
		<!-- End Search Bar -->

		<!-- Start Icons Navigation -->
		<nav class="header-nav ms-auto">
			<ul class="d-flex align-items-center">

				<!-- Start Search Icon-->
				<li class="nav-item d-block d-lg-none">
					<a class="nav-link nav-icon search-bar-toggle" href="#">
						<i class="bi bi-search"></i>
					</a>
				</li>
				<!-- End Search Icon-->

				<!-- Start Profile Nav -->
				<li class="nav-item dropdown pe-3">
					<!-- Start Profile Iamge Icon -->
					<a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
						<img src="resources/person.svg" alt="Profile" class="rounded-circle">
						<span class="d-none d-md-block dropdown-toggle ps-2">Admin Name</span>
					</a>
					<!-- End Profile Iamge Icon -->

					<!-- Start Profile Dropdown Items -->
					<ul class="dropdown-menu dropdown-menu-end profile">
						<li class="dropdown-header">
							<h6>Admin Full Name</h6>
						</li>
						<li>
							<hr class="dropdown-divider">
						</li>

						<li>
							<a class="dropdown-item d-flex align-items-center" href="profileDetails.html">
								<i class="bi bi-person"></i>
								<span>My Profile</span>
							</a>
						</li>
						<li>
							<hr class="dropdown-divider">
						</li>

						<li>
							<a class="dropdown-item d-flex align-items-center" href="#">
								<i class="bi bi-box-arrow-right"></i>
								<span>Sign Out</span>
							</a>
						</li>

					</ul>
					<!-- End Profile Dropdown Items -->
				</li>
				<!-- End Profile Nav -->

			</ul>
		</nav>
		<!-- End Icons Navigation -->
	</header>

	<!-- ======= Sidebar ======= -->
	<aside id="sidebar" class="sidebar">

		<ul class="sidebar-nav" id="sidebar-nav">
			<!-- Start Dashboard Nav -->
			<li class="nav-item">
				<a class="nav-link collapsed" href="index.html">
					<i class="bi bi-grid"></i>
					<span>Dashboard</span>
				</a>
			</li><!-- End Dashboard Nav -->

			<li class="nav-item">
				<a class="nav-link collapsed" data-bs-target="#categories-nav" data-bs-toggle="collapse" href="#">
					<i class="bi bi-menu-button-wide"></i><span>Categories</span><i class="bi bi-chevron-down ms-auto"></i>
				</a>
				<ul id="categories-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
					<li>
						<a href="categoryList.html">
							<i class="bi bi-circle-fill"></i><span>Category List</span>
						</a>
					</li>
				</ul>
			</li>

			<li class="nav-item">
				<a class="nav-link collapsed" data-bs-target="#products-nav" data-bs-toggle="collapse" href="#">
					<i class="bi bi-menu-button-wide"></i><span>Products</span><i class="bi bi-chevron-down ms-auto"></i>
				</a>
				<ul id="products-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
					<li>
						<a href="productList.html">
							<i class="bi bi-circle-fill"></i><span>Product List</span>
						</a>
					</li>
				</ul>
			</li>

			<li class="nav-item">
				<a class="nav-link " data-bs-target="#users-nav" data-bs-toggle="collapse" href="#">
					<i class="bi bi-person"></i>
					<span>Users</span>
					<i class="bi bi-chevron-down ms-auto"></i>
				</a>
				<ul id="users-nav" class="nav-content collapse show" data-bs-parent="#sidebar-nav">
					<li>
						<a href="userList.html" class="active">
							<i class="bi bi-circle-fill"></i><span>User List</span>
						</a>
					</li>
				</ul>
			</li>

			<li class="nav-item">
				<a class="nav-link collapsed" href="login.html">
					<i class="bi bi-box-arrow-in-right"></i>
					<span>Login</span>
				</a>
			</li>

			<li class="nav-item">
				<a class="nav-link collapsed" href="register.html">
					<i class="bi bi-card-list"></i>
					<span>Register</span>
				</a>
			</li>
		</ul>
	</aside><!-- End Sidebar-->

	<main id="main" class="main">

		<div class="pagetitle">
			<h1>View User Statistics</h1>
			<nav>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="userList.html">User List</a></li>
					<li class="breadcrumb-item active">View User Statistics</li>
				</ol>
			</nav>
		</div><!-- End Page Title -->

		<section class="section dashboard">
			<div class="row">
				<div class="col-md-4">
					<div class="card info-card sales-card">

						<div class="card-body">
							<h5 class="card-title">Item Bought</h5>
							<div class="d-flex align-items-center">
								<div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
									<i class="bi bi-cart"></i>
								</div>
								<div class="ps-3">
									<h6>17</h6>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="col-md-4">
					<div class="card info-card sales-card">

						<div class="card-body">
							<h5 class="card-title">Money Spent</h5>
							<div class="d-flex align-items-center">
								<div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
									<i class="bi bi-currency-dollar"></i>
								</div>
								<div class="ps-3">
									<h6>9000</h6>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="col-md-4">
					<div class="card info-card sales-card">

						<div class="card-body">
							<h5 class="card-title">Joined at</h5>
							<div class="d-flex align-items-center">
								<div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
									<i class="bi bi-calendar-event"></i>
								</div>
								<div class="ps-3">
									<h6>September 1, 2022</h6>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="col-12">
					<div class="card top-selling overflow-auto">

						<div class="card-body pb-0">
							<h5 class="card-title">Products bought <span>| Today</span></h5>

							<table class="table table-borderless">
								<thead>
									<tr>
										<th scope="col">Preview</th>
										<th scope="col">Product</th>
										<th scope="col">Price (৳)</th>
										<th scope="col">Sold</th>
										<th scope="col">Revenue (৳)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="row">
											<a href="#">
												<img src="../Picture/Fruites/banana-chompa-ready-to-eat-12-pcs.webp" alt="">
											</a>
										</th>
										<td>Chompa Kola</td>
										<td>70</td>
										<td class="fw-bold">124 Dozen</td>
										<td>5,828</td>
									</tr>
									<tr>
										<th scope="row">
											<a href="#"><img src="../Picture/Fruites/gala-apple-50-gm-1-kg.webp" alt=""></a>
										</th>
										<td>Gala Apple</td>
										<td>290</td>
										<td class="fw-bold">20 Kg</td>
										<td>5,500</td>
									</tr>
									<tr>
										<th scope="row">
											<a href="#"><img src="../Picture/Fruites/kacha-aam-green-mango-50-gm-1-kg.webp" alt=""></a>
										</th>
										<td>Kacha Amm</td>
										<td>50</td>
										<td class="fw-bold">80 Kg</td>
										<td>4,000</td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</section>
	</main>

	<!-- ======= Footer ======= -->
	<footer id="footer" class="footer">
		<div class="copyright">
			Copyright &copy; 2022 All Rights Reserved <strong><span>Batch-007</span></strong>
		</div>
	</footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

	<!-- Bootstrap bundle (CDN) -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
		crossorigin="anonymous"></script>

	<!-- Simple DataTables (CDN) -->
	<script src="https://cdn.jsdelivr.net/npm/simple-datatables@3.2.2/dist/umd/simple-datatables.js"
		integrity="sha256-Usm730G3l59Ux42era3GIRJOYXFLU7K9k7JFInXTeG0=" crossorigin="anonymous"></script>

	<!-- ChartJS (CDN) -->
	<script src="https://cdn.jsdelivr.net/npm/chart.js@3.8.0/dist/chart.min.js"
		integrity="sha256-cHVO4dqZfamRhWD7s4iXyaXWVK10odD+qp4xidFzqTI=" crossorigin="anonymous"></script>

	<!-- Bootstrap bundle (Local) -->
	<!-- <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->

	<!-- Simple DataTables (Local) -->
	<!-- <script src="assets/vendor/simple-datatables/simple-datatables.js"></script> -->

	<!-- ChartJs (Local) -->
	<!-- <script src="assets/vendor/chart.js/chart.min.js"></script> -->

	<!-- Custom JS -->
	<script src="js/main.js"></script>
</body>
</html>
