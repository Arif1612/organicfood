
	<!-- ======= Sidebar ======= -->
<x-admin.layout.master>
	<main id="main" class="main">

		<div class="pagetitle">
			<h1>Category List</h1>
			<nav>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
					<li class="breadcrumb-item active">Category List</li>
				</ol>
			</nav>
		</div><!-- End Page Title -->

		<section class="section">
			<div class="row">
				<div class="col-md-12">
					<div class="card overflow-auto">
						<div class="card-header">
							<div class="row">
								<div class="col-md-6">
									Categories
								</div>
								<div class="col-md-6 d-flex justify-content-end">
									<a class="btn btn-primary mx-2" href="addCategory.html">Add New Category</a>
								</div>
							</div>
						</div>
						<div class="card-body py-2">
							<table class="table table-hover datatable">
								<thead>
									<tr>
										<th scope="col">Preview</th>
										<th scope="col">SL</th>
										<th scope="col">Name</th>
										<th scope="col">Actions</th>
									</tr>
								</thead>
								<!-- <tfoot>
									<tr>
										<th>Preview</th>
										<th>SL</th>
										<th>Name</th>
										<th>Actions</th>
									</tr>
								</tfoot> -->
								<tbody>
									<tr>
										<th scope="row">
											<a href="#">
												<img src="../Picture/fruits.jpg" alt="Fruit">
											</a>
										</th>
										<td>1</td>
										<td>Fruit</td>
										<td>
											<div class="d-flex px-2">
												<a class="btn btn-success mx-2" href="viewCategory.html">View</a>
												<a class="btn btn-primary mx-2" href="editCategory.html">Edit</a>
												<a class="btn btn-danger mx-2" href="deleteCategory.html">Delete</a>
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row">
											<a href="#"><img src="../Picture/vegetable.jpg" alt="Vegetable"></a>
										</th>
										<td>2</td>
										<td>Vegetable</td>
										<td>
											<div class="d-flex px-2">
												<a class="btn btn-success mx-2" href="viewCategory.html">View</a>
												<a class="btn btn-primary mx-2" href="editCategory.html">Edit</a>
												<a class="btn btn-danger mx-2" href="deleteCategory.html">Delete</a>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
</x-admin.layout.master>
	<!-- ======= Footer ======= -->

