<x-admin.layout.master>
    <main id="main" class="main">
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card overflow-auto">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    Products
                                </div>
                                <div class="col-md-6 d-flex justify-content-end">
                                    <a class="btn btn-primary mx-2" href="addProduct.html">Add New Product</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body py-2">
                            <table class="table table-hover datatable">
                                <thead>
                                    <tr>
                                        <th>Preview</th>
                                        <th>SL</th>
                                        <th>Name</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Category Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <!-- <tfoot>
         <tr>
           <th>Preview</th>
           <th>SL</th>
           <th>Name</th>
           <th>Quantity</th>
           <th>Price</th>
           <th>Category Name</th>
           <th>Actions</th>
          </tr>
        </tfoot> -->
                                <tbody>
                                    <tr>
                                        <td><img src="../Picture/Fruites/banana-chompa-ready-to-eat-12-pcs.webp"
                                                alt="Chompa Kola"></td>
                                        <td>1</td>
                                        <td>Chompa Kola</td>
                                        <td>100</td>
                                        <td>70</td>
                                        <td>Fruit</td>
                                        <td>
                                            <div class="d-flex px-2">
                                                <a class="btn btn-success mx-2" href="viewProduct.html">View</a>
                                                <a class="btn btn-primary mx-2" href="editProduct.html">Edit</a>
                                                <a class="btn btn-danger mx-2" href="deleteProduct.html">Delete</a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><img src="../Picture/Fruites/green-apple-50-gm-1-kg.webp" alt="Green Apple">
                                        </td>
                                        <td>2</td>
                                        <td>Green Apple</td>
                                        <td>100</td>
                                        <td>330</td>
                                        <td>Fruit</td>
                                        <td>
                                            <div class="d-flex px-2">
                                                <a class="btn btn-success mx-2" href="viewProduct.html">View</a>
                                                <a class="btn btn-primary mx-2" href="editProduct.html">Edit</a>
                                                <a class="btn btn-danger mx-2" href="deleteProduct.html">Delete</a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</x-admin.layout.master>
