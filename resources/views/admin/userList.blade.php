<x-admin.layout.master>
    <main id="main" class="main">
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card overflow-auto">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    Users
                                </div>
                                <!-- <div class="col-md-6 d-flex justify-content-end">
             <a class="btn btn-primary mx-2" href="addUser.html">Add New User</a>
            </div> -->
                            </div>
                        </div>
                        <div class="card-body py-2">
                            <table class="table table-hover datatable">
                                <thead>
                                    <tr>
                                        <th>Profile</th>
                                        <th>SL</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <!-- <tfoot>
             <tr>
              <th>Profile</th>
              <th>SL</th>
              <th>Name</th>
              <th>Email</th>
              <th>Actions</th>
             </tr>
            </tfoot> -->
                                <tbody>
                                    <tr>
                                        <td><img src="resources/person.svg" alt="User Profile"></td>
                                        <td>1</td>
                                        <td>Lorem ipsum</td>
                                        <td>abs@gmail.com</td>
                                        <td>
                                            <div class="d-flex px-2">
                                                <a class="btn btn-success mx-2" href="viewUser.html">View Statistics</a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><img src="resources/person.svg" alt="User Profile"></td>
                                        <td>2</td>
                                        <td>Doler sum</td>
                                        <td>fgh@gmail.com</td>
                                        <td>
                                            <div class="d-flex px-2">
                                                <a class="btn btn-success mx-2" href="viewUser.html">View Statistics</a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</x-admin.layout.master>


{{-- <div class="pagetitle">
			<h1>User List</h1>
			<nav>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
					<li class="breadcrumb-item active">User List</li>
				</ol>
			</nav> --}}
{{-- </div> --}}
<!-- End Page Title -->
