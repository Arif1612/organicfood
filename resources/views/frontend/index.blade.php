<x-admin.layout.masterFrontend>

    {{-- <!-- Main section/content --> --}}
    <section class="mt-2 mx-md-5">
        <div class="container-md">
            <div class="row">
                <div class="col-md-12 py-3">
                    <!-- Search products by categories -->
                    <div class="row justify-content-center mb-3">
                        <form class="col-md-12" action="" method="get">
                            <div class="input-group mb-3 justify-content-center">
                                <div class="col-md-3 mx-md-2">
                                    <select class="form-select form-control bg-green" name="categories">
                                        <option value="0" selected>All Categories</option>
                                        <option value="1">Vegetables</option>
                                        <option value="2">Fruits</option>
                                        <option value="3">Drinks</option>
                                        <option value="4">Ingridients</option>
                                    </select>
                                </div>

                                <div class="col-md-4 mx-md-2">
                                    <input type="text" class="form-control" placeholder="Search product"
                                        name="search" aria-label="Search product name" aria-describedby="search-all">
                                </div>
                                <div class="col-md-1 mx-md-2">
                                    <button class="btn bg-green" type="submit" id="search-all">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <!-- Landing image and text -->
                    <div class="row position-relative d-flex">
                        <img class="img-fluid" src="resources/images/home.png" alt="">
                        <!-- Absolute positioned text -->
                        <div
                            class="home-text position-absolute d-flex flex-sm-column top-0 end-0 bottom-0 text-end text-white m-sm-auto pe-5 justify-content-center
                    initialism fs-4 letter-spacing-1">
                            <div>
                                <p class="letter-spacing-2">Fresh Food</p>
                            </div>
                            <div>
                                <p class="fs-2"><b>Fresh Vegetable</b></p>
                            </div>
                            <div>
                                <p class="fs-2"><b>100% Organic</b></p>
                            </div>
                            <div>
                                <p class="fs-5">Free Delivery</p>
                            </div>
                            <div>
                                <a href="products.html" class="btn bg-green btn-styled">Shop now</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="col-md-3 py-md-3">
                <div class="accordion" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne"
                                aria-expanded="true" aria-controls="collapseOne">
                                All categories
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                            data-bs-parent="#accordionExample">
                            <div class="accordion-body p-0">
                                <div class="list-group-flush m-md-2">
                                    <a href="#" class="list-group-item list-group-item-action border-0 p-md-3">Vegetables</a>
                                    <a href="#" class="list-group-item list-group-item-action border-0 p-md-3">Fruits</a>
                                    <a href="#" class="list-group-item list-group-item-action border-0 p-md-3">Drinks</a>
                                    <a href="#" class="list-group-item list-group-item-action border-0 p-md-3">Ingridients</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            </div>
        </div>
    </section>

    <!-- Fruite Details  -->
    <div class="container">
        <br>
        <!-- Banana -->
        <h1 class="text-center">Banana</h1>
        <div class="row row-cols-1 row-cols-md-3  g-4">
            <div class="col h-100">
                <div class="card ">
                    <img src="Picture/Fruites/banana-chompa-ready-to-eat-12-pcs.webp" class="card-img-top"
                        alt="...">
                    <div class="card-body btn-light text-center ">
                        <p>Chompa Kola (12pcs)</p>
                        <p>Price 70 ৳</p>
                        <a href="singleProductDetails.html"> <button type="button" class="btn btn-danger ">Add To
                                Cart</button></a>

                    </div>
                </div>
            </div>
            <div class="col h-100">
                <div class="card ">
                    <img src="Picture/Fruites/kacha-kola-banana-green-4-pcs.webp" class="card-img-top" alt="...">
                    <div class="card-body btn-light text-center ">
                        <p>Kacha Kola (5pcs)</p>
                        <p>Price 35 ৳</p>
                        <button type="button" class="btn btn-danger ">Add To Cart</button>
                    </div>
                </div>
            </div>
            <div class="col h-100">
                <div class="card ">
                    <img src="Picture/Fruites/banana-sobri-4-pcs.webp" class="card-img-top" alt="...">
                    <div class="card-body btn-light text-center ">
                        <p>Sobri Kola (5pcs)</p>
                        <p>Price 50 ৳</p>
                        <button type="button" class="btn btn-danger ">Add To Cart</button>
                    </div>
                </div>
            </div>

        </div>
        <!-- Banana End -->

        <!-- Apple Start-->
        <br>
        <h1 class="text-center">Apple</h1>
        <div class="row row-cols-1 row-cols-md-3  g-4">
            <div class="col h-100">
                <div class="card ">
                    <img src="Picture/Fruites/green-apple-50-gm-1-kg.webp" class="card-img-top" alt="...">
                    <div class="card-body btn-light text-center ">
                        <p>Green Apple (1 kg)</p>
                        <p>Price 330 ৳</p>
                        <button type="button" class="btn btn-danger ">Add To Cart</button>
                    </div>
                </div>
            </div>
            <div class="col h-100">
                <div class="card ">
                    <img src="Picture/Fruites/gala-apple-50-gm-1-kg.webp" class="card-img-top" alt="...">
                    <div class="card-body btn-light text-center ">
                        <p>Gala Apple (1kg)</p>
                        <p>Price 290 ৳</p>
                        <button type="button" class="btn btn-danger ">Add To Cart</button>
                    </div>
                </div>
            </div>
            <div class="col h-100">
                <div class="card ">
                    <img src="Picture/Fruites/china fuji.webp" class="card-img-top" alt="...">
                    <div class="card-body btn-light text-center ">
                        <p>China Fuji Apple (1kg)</p>
                        <p>Price 280 ৳</p>
                        <button type="button" class="btn btn-danger ">Add To Cart</button>
                    </div>
                </div>
            </div>

        </div>


        <!-- Apple end -->

        <!-- Mango Start -->
        <br>
        <h1 class="text-center">Mango</h1>
        <div class="row row-cols-1 row-cols-md-3  g-4">
            <div class="col h-100">
                <div class="card ">
                    <img height="414px" width="414px" src="Picture/Fruites/kacha-aam-green-mango-50-gm-1-kg.webp"
                        class="card-img-top" alt="...">
                    <div class="card-body btn-light text-center ">
                        <p>Kacha Amm (1kg)</p>
                        <p>Price 50 ৳</p>
                        <button type="button" class="btn btn-danger ">Add To Cart</button>
                    </div>
                </div>
            </div>
            <div class="col h-100">
                <div class="card ">
                    <img height="414px" width="414px" src="Picture/Fruites/Harivanga-Mango.png"
                        class="card-img-top" alt="...">
                    <div class="card-body btn-light text-center ">
                        <p>Harivanga Amm (1kg)</p>
                        <p>Price 150 ৳</p>
                        <button type="button" class="btn btn-danger ">Add To Cart</button>
                    </div>
                </div>
            </div>
            <div class="col h-100">
                <div class="card ">
                    <img height="414px" width="414px" src="Picture/Fruites/himsagor.jpg" class="card-img-top"
                        alt="...">
                    <div class="card-body btn-light text-center ">
                        <p>Himsagor (1kg)</p>
                        <p>Price 80 ৳</p>
                        <button type="button" class="btn btn-danger ">Add To Cart</button>
                    </div>
                </div>
            </div>

        </div>

        <!-- Mango End -->
    </div>

</x-admin.layout.masterFrontend>
