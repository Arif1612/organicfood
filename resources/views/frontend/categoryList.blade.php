<x-admin.layout.masterFrontend>
    <!-- Main Part -->
    <div class="container">
        <h1 class="text-center">Product Category</h1>
        <br>
        <div class="row row-cols-1 row-cols-md-2 g-4">
            <!-- VEGETABLES -->
            <div class="col h-100">
                <div class="card  ">
                    <img src="Picture/vegetable.jpg" class="card-img-top" alt="...">
                    <div class="card-body text-center ">
                        <button type="button" class="btn btn-danger ">VEGETABLE DETAILS</button>
                    </div>
                </div>
            </div>

            <!-- FRUITS -->

            <div class="col">
                <div class="card h-100 ">
                    <img src="Picture/fruits.jpg" class="card-img-top" alt="...">
                    <div class="card-body text-center ">
                        <a href="fruiteDetails.html"><button type="button" class="btn btn-danger ">FRUITS</button></a>

                    </div>
                </div>
            </div>

            <!-- DRINKS -->

            <div class="col">
                <div class="card h-100">
                    <img src="Picture/drinks.png" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title text-center">DRINKS</h5>
                    </div>
                </div>
            </div>

            <!-- INGREDIENTS -->

            <div class="col">
                <div class="card h-100">
                    <img src="Picture/ingredients2.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title text-center">INGREDIENTS</h5>

                    </div>
                </div>
            </div>

        </div>
    </div>
</x-admin.layout.masterFrontend>
