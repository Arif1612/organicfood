<x-admin.layout.masterFrontend>
    <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top border-bottom">
        <div class="container">
            <div class="me-0">
                <!-- Brand image -->
                <a class="navbar-brand" href="index.html">
                    <img src="resources/images/logo.png" alt="Organic Food" height="56px">
                </a>
            </div>
            <!-- Collapseable nav bar options-->
            <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="index.html">Home</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            Categories
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item" href="vegetables.html">Vegetables</a></li>
                            <li><a class="dropdown-item" href="fruiteDetails.html">Fruits</a></li>
                            <li><a class="dropdown-item" href="drinks.html">Drinks</a></li>
                            <li><a class="dropdown-item" href="ingridients.html">Ingridients</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="categoryList.html">Products</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="aboutUs.html">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contactUs.html">Contact Us</a>
                    </li>
                </ul>
            </div>

            <!-- Cart, favourites and nav bar toggler button -->
            <div class="justify-content-center">
                <div class="d-flex justify-content-center pt-3">

                    <a href="favourites.html" class="btn btn-outline-primary position-relative ms-2 me-3">
                        <i class="fa-solid fa-heart"></i>
                        <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                            99+
                            <span class="visually-hidden">Items</span>
                        </span>
                    </a>

                    <a href="cart.html" class="btn btn-outline-primary position-relative ms-3 me-2">
                        <i class="fa fa-cart-shopping"></i>
                        <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                            99+
                            <span class="visually-hidden">Items</span>
                        </span>
                    </a>

                    <button class="navbar-toggler ms-3 me-2" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
            </div>
        </div>
    </nav>
    <div class="container overflow-hidden">
        <div class="row my-3">
            <div class="col-6">
                <div class="bg-light shadow p-3 bg-body rounded">
                    <img width="100%" src="Picture/Fruites/banana-chompa-ready-to-eat-12-pcs.webp" alt="">

                    <!-- owl carousel added -->
                    <div class="owl-carousel owl-theme container ">
                        <div class="item ">
                            <img height="200px" src="Picture/Fruites/banana-sobri-4-pcs.webp" alt="">
                        </div>
                        <div class="item">
                            <img height="200px" src="Picture/Fruites/china fuji.webp" alt="">
                        </div>
                        <div class="item">
                            <img height="200px" src="Picture/Fruites/gala-apple-50-gm-1-kg.webp" alt="">
                        </div>
                        <div class="item">
                            <img height="200px" src="Picture/Fruites/green-apple-50-gm-1-kg.webp" alt="">
                        </div>
                        <div class="item">
                            <img height="200px" src="Picture/thumb-1.jpg" alt="">
                        </div>
                        <div class="item">
                            <img height="200px" src="Picture/thumb-2.jpg" alt="">
                        </div>
                        <div class="item">
                            <img height="200px" src="Picture/thumb-3.jpg" alt="">
                        </div>
                        <div class="item">
                            <img height="200px" src="Picture/Fruites/green-apple-50-gm-1-kg.webp" alt="">
                        </div>

                    </div>
                </div>
            </div>
            <!-- 2nd col -->
            <div class="col-6 ps-5">
                <div class=" ">
                    <h1>CHOMPA KOLA</h1>
                    <div class="text-warning ">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half"><span id="halfStar">(22 reviews)</span> </i>
                    </div>
                    <h2>70 ৳</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam rerum facilis similique, quasi
                        illum, dolorem quas odit veritatis ab perspiciatis voluptatem quo itaque ipsum enim maiores
                        earum minus animi eligendi!
                    </p>

                    <div class="input-group">
                        <select class="form-select" id="inputGroupSelect04"
                            aria-label="Example select with button addon">
                            <option selected>Quantity</option>
                            <option value="1">1 dozon</option>
                            <option value="2">2 dozon</option>
                            <option value="3">3 dozon</option>
                        </select>
                        <a href="addToCart.html"> <button class="btn btn-primary" type="button">Add To
                                Cart</button></a>

                    </div>
                    <br><br>
                    <div class="p-2">
                        <table>
                            <tr>
                                <th class="pe-5">Availability: </th>
                                <td>In Stock</td>
                            </tr>
                            <tr>
                                <th>Shipping: </th>
                                <td>01 day Shipping</td>
                            </tr>
                            <tr>
                                <th>Weight: </th>
                                <td>0.5 kg</td>
                            </tr>
                            <tr>
                                <th>Share on: </th>
                                <td>
                                    <a class="pe-3" target="_blank" href="https://www.facebook.com/"> <i
                                            class="fa fa-facebook"></i></a>
                                    <a class="pe-3" target="_blank" href="https://twitter.com/"> <i
                                            class="fa fa-twitter"></i></a>
                                    <a target="_blank" href=""> <i class="fa fa-instagram"></i></a>


                                </td>
                            </tr>
                        </table>
                    </div>


                </div>
            </div>
        </div>
        <!-- another row -->
        <div class="d-flex justify-content-evenly p-5 m-5 discription">
            <div>
                <a href="">Description</a>
            </div>
            <div>
                <a href="">Information</a>
            </div>
            <div>
                <a href="">Reviews</a>
            </div>
        </div>

        <!-- row product information -->
        <div class="row">
            <br><br><br>
            <h2 class="text-center">Product Information</h2>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ea architecto eum consequatur optio magni
                reiciendis praesentium assumenda excepturi, doloremque nesciunt perferendis voluptate cum repellat quas.
                Magnam repellat nihil vitae soluta rem sequi molestiae dolor enim. Eum, velit fugit, pariatur et
                voluptas cupiditate asperiores, in nihil architecto quam dolorum? Aspernatur quos numquam eius rerum
                maxime rem dicta earum maiores obcaecati quasi!</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae perferendis fuga quae nobis quis autem
                saepe,
                doloribus quidem pariatur sit, similique voluptatibus dolorem officia dolores aspernatur deserunt
                distinctio
                non praesentium.</p>
        </div>
        <br><br>
        <!-- Related Product Row -->
        <div class="row">
            <h2 class="text-center">Related Product</h2>
            <div class="row row-cols-1 row-cols-md-3 g-4">
                <div class="col">
                    <div class="card">
                        <img src="Picture/Fruites/kacha-kola-banana-green-4-pcs.webp" class="card-img-top"
                            alt="...">
                        <div class="card-body">
                            <h5 class="card-title text-center">KACHA KOLA</h5>
                            <h3 class="text-center">60 ৳</h3>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <img src="Picture/Fruites/gala-apple-50-gm-1-kg.webp" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title text-center">GALA APPLE</h5>
                            <h3 class="text-center">260 ৳</h3>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <img src="Picture/Fruites/green-apple-50-gm-1-kg.webp" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title text-center">GREEN APPLE</h5>
                            <h3 class="text-center">280 ৳</h3>
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <br><br>
    </div>
</x-admin.layout.masterFrontend>
