<x-admin.layout.masterFrontend>
    <!-- -------------------------------
        ------------------------
        Main section/content
    --------------------------------------
         ----------------------->

    <section class="container">
        <div class="row">
            <!-- Billing Details -->
            <div class="col-9">
                <div class="d-flex flex-md-column vh-100">
                    <div class="container-md py-md-5">
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <form class="shadow-lg rounded-xl mt-3 p-5 bg-white" action="" method="GET">
                                    <div class="col-form-label text-center pb-4">
                                        <h4>Billing Details</h4>
                                    </div>

                                    <div class="form-floating mb-3">
                                        <input name="fname" type="text" class="form-control" id="fname"
                                            placeholder="" autofocus required>
                                        <label for="fname">First name</label>
                                    </div>

                                    <div class="form-floating mb-3">
                                        <input name="lname" type="text" class="form-control" id="lname"
                                            placeholder="" required>
                                        <label for="lname">Last name</label>
                                    </div>

                                    <div class="form-floating mb-3">
                                        <input name="email" type="email" class="form-control" id="user_email"
                                            placeholder="name@example.com" required>
                                        <label for="user_email">Email address</label>
                                    </div>

                                    <div class="form-floating mb-3">
                                        <input type="text" class="form-control" id="phone_number" name="phone_number"
                                            placeholder="" required>
                                        <label for="phone_number">Phone Number</label>
                                    </div>

                                    <div class="form-floating mb-3">
                                        <textarea class="form-control" id="address" name="address" placeholder="" required></textarea>
                                        <label for="address">Address</label>
                                    </div>

                                    <div class="row p-3">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="card"
                                                id="bkash_check">
                                            <label class="form-check-label" for="bkash_check">
                                                bKash
                                            </label>
                                        </div>
                                        <!-- <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked">
                                        <h5 class="text-dark">Check Payment</h5>
                                        </label> -->
                                    </div>
                                    <div class="row p-3">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="card"
                                                id="visa_check">
                                            <label class="form-check-label" for="visa_check">
                                                VISA Card
                                            </label>
                                        </div>
                                        <!-- <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked">
                                        <h5 class="text-dark">
                                            Visa Card
                                        </h5>
                                        </label> -->
                                    </div>

                                    <div class="form-floating mb-3">
                                        <input type="text" class="form-control" id="card_number" name="card_number"
                                            placeholder="" required>
                                        <label for="card_number">Card Number</label>
                                    </div>

                                    <div class="row mb-3">
                                        <div class="col-md-12 text-center">
                                            <!-- <button type="submit" class="btn btn-primary">Place Order</button> -->
                                            <button type="button" class="btn btn-success" data-bs-toggle="modal"
                                                data-bs-target="#staticBackdrop">Place Order</button>
                                        </div>
                                    </div>

                                    <!-- <div class="row text-center">
                                        <a class="text-decoration-none cursor-pointer" href="login.html">Have an
                                            account? Login now</a>
                                    </div> -->

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Your Order -->
            <div class="col-3">
                <br><br>
                <h1>Your Order</h1>
                <div class="row">
                    <table>
                        <tr>
                            <th class="p-3">
                                <h3>Products</h3>
                            </th>
                            <th class="p-3">
                                <h3>Total</h3>
                            </th>
                        </tr>
                        <tr>
                            <td class="p-3">Vegetable Package</td>
                            <td class="p-3">450 ৳</td>
                        </tr>

                        <tr>
                            <td class="p-3">Chompa Kola</td>
                            <td class="p-3">70 ৳</td>
                        </tr>
                        <tr>
                            <td class="p-3">Harivanga</td>
                            <td class="p-3">300 ৳</td>
                        </tr>
                        <hr>
                        <tr>
                            <th class="p-3">
                                <h3>Subtotal</h3>
                            </th>
                            <th class="p-3">
                                <h3>820 ৳</h3>
                            </th>
                        </tr>

                        <tr>

                            <th class="p-3">
                                <h3>Total</h3>
                            </th>
                            <th class="p-3">
                                <h3>820 ৳</h3>
                            </th>
                        </tr>

                    </table>

                    <!-- <div class="row p-3">
                        <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked">
                        <label class="form-check-label text-light rounded" for="flexCheckChecked">
                            <h5 class="text-dark"> Create An Account</h5>
                            <p class="text-dark">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eius iste
                                adipisci quae
                                quisquam nostrum a doloribus voluptatem quaerat aspernatur vel ipsam labore est ut ab
                                asperiores perferendis nulla, corrupti quia?</p>

                        </label>
                    </div> -->
                </div>
                <div class="row fs-4 w-75 p-3">

                    <!-- Button trigger modal -->
                    <!-- <button type="button" class="btn-success" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                        PLACE ORDER
                    </button> -->

                    <!-- Modal -->
                    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false"
                        tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="staticBackdropLabel">Thank You For Shoping</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    ...
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Close</button>
                                    <a href="invoice.html">
                                        <button type="button" class="btn btn-primary">INVOICE</button>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

    </section>
</x-admin.layout.masterFrontend>
